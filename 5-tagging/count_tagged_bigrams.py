from pygtrie import CharTrie
import json, os, re, sys

TAGGED_PATTERN = re.compile('\t([^\s]*)\t([^\s:]*).*')

def is_good_word(word, cat):
    return len(word) > 0 and not re.match(r'\d+', word) and cat != 'interp'

def words_gen(f):
    for line in f.readlines():
        match = TAGGED_PATTERN.match(line)
        if match != None:
            word, cat = match.groups()
            if is_good_word(word, cat):
                yield (word, cat)


def persist_freq_list(freq_trie, freq_file):
    for count, bigram in sorted(((freq_trie[bigram], bigram) for bigram in freq_trie), reverse=True):
        line = '{} {}\n'.format(count, bigram)
        freq_file.write(line)


def main():
    if len(sys.argv) < 2:
        print('Data directory path needed.')
        sys.exit(-1)

    freq_trie = CharTrie()

    for dirname, _, filenames in os.walk(sys.argv[1]):
        for filename in filenames:
            with open(os.path.join(dirname, filename)) as f:
                words = list(words_gen(f))
                for (w1, c1), (w2, c2) in zip(words, words[1:]):
                    bigram = '{}:{} {}:{}'.format(w1.lower(), c1, w2.lower(), c2)
                    prev = freq_trie.get(bigram)
                    freq_trie[bigram] = prev + 1 if prev else 1

    with open('tagged_bigram_freq.txt', 'w') as freq_file:
        persist_freq_list(freq_trie, freq_file)


if __name__ == '__main__':
    main()

