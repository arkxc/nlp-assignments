# Wyniki przetwarzania zbioru danych dla 2006 roku

1. Najczęściej występujące bigramy o postaci, w których na pierszym miejscu występuje rzeczownik, a na drugim miejscu rzeczownik lub przymiotnik:

    - sąd wysoki
    - skarga kasacyjny
    - trybunał konstytucyjny
    - sąd okręgowy
    - sąd apelacyjny
    - ubezpieczenie społeczny
    - skarb państwo
    - dzień grudzień
    - dzień czerwiec
    - sąd rejonowy
    - posiedzenie niejawny
    - skarga konstytucyjny
    - dzień lipiec
    - wyrok sąd
    - rzeczpospolita polski
    - dzień październik
    - dzień styczeń
    - stosunek praca
    - dzień kwiecień
    - dzień listopad
    - dzień marzec
    - działalność gospodarczy
    - dzień luty
    - dzień maj
    - dzień sierpień
    - dzień wrzesień
    - opieka zdrowotny
    - sąd pierwszy
    - prokurator generalny
    - strona powodowy
