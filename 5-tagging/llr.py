from math import log2
from operator import itemgetter

import collections
import numpy as np


def H(k):
    N = np.sum(k)
    hs = (x/N * log2(x/N + (x == 0)) for x in k.flat)
    return sum(hs)

def colSums(k):              
    return np.sum(k, axis=0)

def rowSums(k):
    return np.sum(k, axis=1)

def LLR(k):
    return 2 * np.sum(k) * (H(k) - H(rowSums(k)) - H(colSums(k)))


def add(t1, *ts):
    a, b = t1
    for c, d in ts:
        a += c
        b += d
    return (a, b)

def update_dict(in_dict, wordA, wordB, count):
    ab, ba = in_dict.get(wordA, {}).get(wordB, (0, 0))
    in_dict[wordA][wordB] = (ab + count, ba)
    in_dict[wordB][wordA] = (ba, ab + count)
    
    in_dict['ALL']        += count
    in_dict[wordA]['ALL'] = add((count, 0), in_dict[wordA]['ALL'])
    in_dict[wordB]['ALL'] = add((0, count), in_dict[wordB]['ALL'])


def matrix_for(in_dict, wordA, wordB):
    ALL = in_dict['ALL']
    A = in_dict[wordA]['ALL'][0]
    B = in_dict[wordB]['ALL'][1]

    A_AND_B = in_dict[wordA][wordB][0]
    A_NOT_B = A - A_AND_B
    B_NOT_A = B - A_AND_B
    NEITHER = ALL - A - B + A_AND_B

    return np.array([
        [A_AND_B, B_NOT_A],
        [A_NOT_B, NEITHER]
    ])


def main():
    in_dict = collections.defaultdict(lambda: {'ALL': (0, 0)})
    in_dict['ALL'] = 0
    out_dict = {}

    with open('tagged_bigram_freq.txt', 'r') as in_file:
        for line in in_file.readlines():
            count, wordA, wordB = line.split()
            bigram = wordA + ' ' + wordB
            print('Collecting: ' + bigram)
            update_dict(in_dict, wordA, wordB, int(count))

    for wordA, counts in in_dict.items():
        if wordA == 'ALL': continue
        for wordB in counts:
            if wordB == 'ALL': continue
            bigram = wordA + ' ' + wordB
            print('Processing: ' + bigram)
            matrix = matrix_for(in_dict, wordA, wordB)
            out_dict[bigram] = LLR(matrix)

    with open('tagged_bigram_llr.txt', 'w') as out_file:
        for bigram, llr in sorted(out_dict.items(), key=itemgetter(1), reverse=True):
            print('Saving: ' + bigram)
            out_file.write('{} {}\n'.format(llr, bigram))


if __name__ == '__main__':
    main()

