import json, os, sys, requests
import time

def analyze(text):
    return requests.post("http://localhost:9200/", data=text.encode('utf-8')).text


def analyze_and_save(data):
    for item in data['items']:
        year = item['judgmentDate'][:4]
        if year == '2006':
            id = item['id']
            content = item['textContent']
            with open(os.path.join('tagged', str(id) + '.txt'), 'w') as f:
                f.write(analyze(content))


def main():
    if len(sys.argv) < 2:
        print('Data directory path needed.')
        sys.exit(-1)

    start = time.time()

    for dirname, _, filenames in os.walk(sys.argv[1]):
        n = len(filenames)
        for i, filename in enumerate(filenames):
            if filename.startswith('judgments-') and filename.endswith('.json'):
                print('Processing: {} / {}'.format(i + 1, n))
                data = json.load(open(os.path.join(dirname, filename)))
                analyze_and_save(data)

    end = time.time()
    print(end - start)


if __name__ == '__main__':
    main()

