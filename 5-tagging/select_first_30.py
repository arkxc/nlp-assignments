import re

PATTERN = re.compile('\d+\.\d+ ([^:]+):subst ([^:]+):(?:subst|adj|adja|adjp|adjc)')

def main():
    left = 30
    with open('tagged_bigram_llr.txt', 'r') as f:
        for line in f.readlines():
            match = PATTERN.match(line)
            if match:
                w1, w2 = match.groups()
                print(w1, w2)
                left -= 1
            if left == 0: break

if __name__ == '__main__':
    main()

