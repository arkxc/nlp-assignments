# Wyniki przetwarzania zbioru danych dla 2006 roku

1. Histogram przedstawiający rozkład wartości pieniężnych:

    ![](money-results.png)

2. Histogramy dla przedziałów poniżej i powyżej 1 mln złotych:

    ![](money-lt1M-results.png)
    ![](money-ge1M-results.png)

3. Liczba orzeczeń odwołujących się do _artykułu 445 Ustawy z dnia 23 kwietnia 1964 r. - Kodeks cywilny_: **21**
4. Liczba orzeczeń zawierających słowo _szkoda_: **445**

# Zadanie dodatkowe

1. Liczba wartości pieniężnych z roku 2004: **2229**
