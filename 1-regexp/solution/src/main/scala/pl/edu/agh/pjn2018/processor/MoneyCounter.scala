package pl.edu.agh.pjn2018.processor

import java.io.File

import akka.Done
import akka.stream.scaladsl.Sink
import pl.edu.agh.pjn2018.{StreamLauncher, Utils}

import scala.concurrent.Future

trait MoneyCounter extends JudgmentsProcessor {
  this: StreamLauncher =>

  import Utils._

  def countMoneyInJudgmentsFromYear(year: Int)(rootData: File): Future[Done] =
    judgmentContentsFromYearSource(year)(rootData)
      .mapAsync(8)(countMoney)
      .fold(0) { (acc, x) => acc + x }
      .map(_.toString)
      .runWith(Sink.foreach(println))

  private def countMoney(content: String): Future[Int] =
    Future {
      ultimateMoneyRegex.findAllMatchIn(content)
        .foldLeft(0)((acc, _) => acc + 1)
    }

}
