package pl.edu.agh.pjn2018.processor

import java.io.File

import akka.Done
import akka.stream.scaladsl.Sink
import pl.edu.agh.pjn2018.StreamLauncher

import scala.concurrent.Future
import scala.util.matching.Regex

trait WordCounter extends JudgmentsProcessor {
  this: StreamLauncher =>

  private val wordRegex: Regex = """\bszk(?:oda|ody|odzie|odę|odą|odo|ód|odom|odami|odach)\b""".r.unanchored

  def countWordOccurrencesInYear(year: Int)(rootData: File): Future[Done] =
    judgmentContentsFromYearSource(year)(rootData)
      .mapAsync(8)(countWordOccurrences)
      .fold(0) { (acc, x) => acc + x }
      .map(i => s"Judgments matching criteria: $i")
      .runWith(Sink.foreach(println))

  private def countWordOccurrences(content: String): Future[Int] =
    Future {
      if (wordRegex.findAllMatchIn(content).isEmpty) 0 else 1
    }

}
