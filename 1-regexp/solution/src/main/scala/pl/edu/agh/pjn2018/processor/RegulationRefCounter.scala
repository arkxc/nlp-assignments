package pl.edu.agh.pjn2018.processor

import java.io.File

import akka.Done
import akka.stream.scaladsl.Sink
import cats.implicits._
import cats.kernel.Monoid
import pl.edu.agh.pjn2018.{Item, StreamLauncher}

import scala.concurrent.Future

trait RegulationRefCounter extends JudgmentsProcessor {
  this: StreamLauncher =>

  import RegulationRefCounter._

  private val longTitle   = "Ustawa z dnia 23 kwietnia 1964 r. - Kodeks cywilny"
  private val inTextRegex = """455\s*?k(?:\s*?|.)?c(?:\s*?|.)?""".r.unanchored

  def countReferencesInYear(year: Int)(rootData: File): Future[Done] =
    judgmentsFromYearSource(year)(rootData)
      .mapAsync(8)(countReferences)
      .fold(RegulationRefCount()) { (acc, x) => acc |+| x }
      .map(_.stats)
      .runWith(Sink.foreach(println))

  private def countReferences(judgment: Item): Future[RegulationRefCount] =
    Future {
      val foundInText = inTextRegex.findAllMatchIn(judgment.textContent).nonEmpty
      val foundInRegulations = judgment.referencedRegulations.exists { r =>
        r.journalTitle == longTitle && r.text.contains("art. 455")
      }
      RegulationRefCount(foundInText, foundInRegulations)
    }

}

object RegulationRefCounter {

  final case class RegulationRefCount(
    foundInTextOnly: Int = 0,
    foundInRegulationsOnly: Int = 0,
    foundInBoth: Int = 0
  ) {

    def stats: String =
      s"""Regulations found:
         | * in text only:        $foundInTextOnly
         | * in regulations only: $foundInRegulationsOnly
         | * in both:             $foundInBoth
      """.stripMargin

  }

  object RegulationRefCount {

    // @formatter:off

    def apply(foundInText: Boolean, foundInRegulations: Boolean): RegulationRefCount =
      (foundInText, foundInRegulations) match {
        case (true,   true) => RegulationRefCount(0, 0, 1)
        case (false,  true) => RegulationRefCount(0, 1)
        case (true,  false) => RegulationRefCount(1)
        case (false, false) => RegulationRefCount()
      }

    // @formatter:on

    implicit val RegulationRefCountMonoid: Monoid[RegulationRefCount] =
      new Monoid[RegulationRefCount] {
        def empty: RegulationRefCount = RegulationRefCount()

        def combine(x: RegulationRefCount, y: RegulationRefCount): RegulationRefCount =
          (x, y) match {
            case (RegulationRefCount(t1, r1, b1), RegulationRefCount(t2, r2, b2)) =>
              RegulationRefCount(t1 + t2, r1 + r2, b1 + b2)
          }
      }

  }

}
