package pl.edu.agh.pjn2018

import java.io.File

import cats.implicits._

import scala.util.matching.Regex

object Utils {

  val judgmentDateKey = "judgmentDate"

  def allFiles(root: File): List[File] =
    if (root.exists && root.isDirectory)
      root.listFiles.filter(_.isFile).toList
    else Nil

  def isJudgmentJson(file: File): Boolean =
    file.isFile && (file.getName startsWith "judgments") && (file.getName endsWith ".json")

  def isJudgmentFrom(year: Int)(judgment: Item): Boolean =
    judgment.judgmentDate.take(4) == year.toString

  def moneyMatchToValue(money: Regex.Match): BigDecimal = {
    def amountToDecimal(amount: String): BigDecimal = {
      def parseDecimal(str: String) =
        Either.catchOnly[NumberFormatException](BigDecimal(str.toLong))

      val isDecimal = (amount contains ",") && !(amount contains "-")
      val parsed = parseDecimal(amount filter (_.isDigit))

      parsed.fold(_ => BigDecimal(0), n => if (isDecimal) n / 100 else n)
    }

    // @formatter:off

    def multiplierToDecimal(mult: String): BigDecimal = mult match {
      case "tys" | "tys." | "tysiąc"  | "tysiące"  | "tysięcy"   => 1000L
      case "mln" | "mln." | "milion"  | "miliony"  | "milionów"  => 1000000L
      case "mld" | "mld." | "miliard" | "miliardy" | "miliardów" => 1000000000L
    }

    // @formatter:on

    val amount = amountToDecimal(money.group(1))
    Option(money.group(2)).fold(amount)(amount * multiplierToDecimal(_))
  }

  def findMoney(roundToIntervalStart: BigDecimal => BigDecimal)(content: String): Occurrences[Interval] = {
    def updateIntervals(acc: Occurrences[Interval], x: Regex.Match) = {
      val interval = roundToIntervalStart(moneyMatchToValue(x))
      acc.get(interval)
        .fold(acc.updated(interval, 1)) { n => acc.updated(interval, n + 1) }
    }

    ultimateMoneyRegex.findAllMatchIn(content)
      .foldLeft(emptyOccurrences[Interval])(updateIntervals)
  }

  val ultimateMoneyRegex: Regex = {
    val amount = {
      val head = """\d{1,3}"""
      val tail = """(?:[. \n]?\d{3})*"""
      val maybeDecimal = """(?: ?, ?(?:-|\d{2}))?"""
      head + tail + maybeDecimal
    }

    val multiplier = {
      val kilo = """tys[.]?|tysi(?:ąc[e]?|ęcy)?"""
      val mega = """mln[.]?|milion(?:y|ów)?"""
      val giga = """mld[.]?|miliard(?:y|ów)?"""

      kilo + "|" + mega + "|" + giga
    }

    val currency = """zł(?:.?|ote|otych)"""

    val maybeInWords = """(?:\([^\)]*?\))?"""

    val maybeSep = """\s*?"""

    s"""(?:($amount)(?:$maybeSep($multiplier)?))(?=$maybeSep$maybeInWords$maybeSep$currency)""".r.unanchored
  }

}
