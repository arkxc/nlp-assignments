package pl.edu.agh.pjn2018.processor

import java.io.File
import java.nio.file.Paths

import akka.NotUsed
import akka.stream.IOResult
import akka.stream.scaladsl.{FileIO, Flow, Keep, Sink, Source}
import akka.util.ByteString
import pl.edu.agh.pjn2018._

import scala.concurrent.Future

trait JudgmentsProcessor {
  this: StreamLauncher =>

  import Utils._

  def judgmentContentsFromYearSource(year: Int)(dataRoot: File): Source[String, NotUsed] =
    filesSource(dataRoot)
      .mapConcat(content => content.items.filter(isJudgmentFrom(year)).map(_.textContent))

  def judgmentsFromYearSource(year: Int)(dataRoot: File): Source[Item, NotUsed] =
    filesSource(dataRoot)
      .mapConcat(content => content.items.filter(isJudgmentFrom(year)))

  def filesSource(dataRoot: File): Source[FileContent, NotUsed] =
    Source(allFiles(dataRoot))
      .filter(isJudgmentJson)
      .mapAsync(8)(JsonParser.fromFileAsync)
      .filter(_.isRight)
      .map(_.right.get)

  def fileSink(filename: String): Sink[String, Future[IOResult]] =
    Flow[String]
      .map(s => ByteString(s + "\n"))
      .toMat(FileIO.toPath(Paths.get(filename)))(Keep.right)

}
