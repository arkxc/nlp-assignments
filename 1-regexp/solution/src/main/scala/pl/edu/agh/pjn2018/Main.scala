package pl.edu.agh.pjn2018

import java.io.{File, FileNotFoundException}

import akka.actor.ActorSystem
import akka.event.Logging
import akka.stream.ActorMaterializer
import cats.implicits._
import pl.edu.agh.pjn2018.processor._

import scala.concurrent.ExecutionContext

trait NLPRegexpProcessor
  extends StreamLauncher
    with MoneyFinder with RoundingRule
    with RegulationRefCounter
    with WordCounter
    with MoneyCounter {

  implicit val sys: ActorSystem       = ActorSystem("nlp-regexp-system")
  implicit val mat: ActorMaterializer = ActorMaterializer()
  implicit val ec : ExecutionContext  = sys.dispatcher

  val groupingIntervalLength = 10000

  def millis = System.currentTimeMillis

  val start = millis

  def run(cfg: ProcessingConfig): Unit = {
    import ProcessingTask._

    val taskStream =
      cfg.task match {
        case GroupMoney =>
          findMoneyInJudgmentsFromYear(cfg.fromYear)(cfg.dataRoot)

        case CountReferencesToRegulation =>
          countReferencesInYear(cfg.fromYear)(cfg.dataRoot)

        case CountWordOccurrences =>
          countWordOccurrencesInYear(cfg.fromYear)(cfg.dataRoot)

        case CountMoneyOccurrences =>
          countMoneyInJudgmentsFromYear(cfg.fromYear)(cfg.dataRoot)
      }

    taskStream foreach { done =>
      println(s"time: ${millis - start}")
      sys.terminate()
    }
  }

}

object Main extends App with NLPRegexpProcessor {

  val log = Logging(sys.eventStream, getClass)

  if (args.length < 3) {
    log.error("Provide task to perform, year to lookup and data directory as arguments")
    System.exit(-1)
  }

  val task = args(0) match {
    case "m" | "money" =>
      ProcessingTask.GroupMoney.asRight[RuntimeException]
    case "cr" | "countref" =>
      ProcessingTask.CountReferencesToRegulation.asRight[RuntimeException]
    case "cw" | "countword" =>
      ProcessingTask.CountWordOccurrences.asRight[RuntimeException]
    case "cm" | "countmoney" =>
      ProcessingTask.CountMoneyOccurrences.asRight[RuntimeException]
    case any =>
      new RuntimeException(s"Not a processing task: $any").asLeft[ProcessingTask]
  }

  val fromYear = Either.catchOnly[NumberFormatException](args(1).toInt)

  val dataDir: Either[FileNotFoundException, File] = {
    val f = new File(args(2))
    if (f.exists && f.isDirectory) f.asRight[FileNotFoundException]
    else new FileNotFoundException(s"File does not exist or is not a directory: ${f.getAbsolutePath}").asLeft[File]
  }

  val cfgOrErr: Either[Exception, ProcessingConfig] =
    (task, fromYear, dataDir).mapN(ProcessingConfig)

  cfgOrErr match {
    case Right(cfg) => run(cfg)
    case Left(err) =>
      log.error(err, "Invalid arguments passed to program")
      sys.terminate()
  }

}
