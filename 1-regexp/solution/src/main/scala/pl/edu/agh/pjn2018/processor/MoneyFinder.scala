package pl.edu.agh.pjn2018.processor

import java.io.File

import akka.stream.IOResult
import cats.implicits._
import pl.edu.agh.pjn2018._

import scala.concurrent.Future
import scala.math.BigDecimal.RoundingMode

trait RoundingRule {

  def groupingIntervalLength: Int

  def roundToIntervalStart(amount: BigDecimal): BigDecimal = {
    val rounded = amount.setScale(0, RoundingMode.CEILING).toBigInt
    BigDecimal(rounded / groupingIntervalLength * groupingIntervalLength)
  }

}

trait MoneyFinder extends JudgmentsProcessor {
  this: StreamLauncher with RoundingRule =>

  import Utils._

  def findMoneyInJudgmentsFromYear(year: Int)(rootData: File): Future[IOResult] =
    judgmentContentsFromYearSource(year)(rootData)
      .mapAsync(8)(findMoneyAsync)
      .fold(emptyOccurrences[Interval]) { (acc, x) => acc |+| x }
      .mapConcat(stringifyOccurrences)
      .runWith(fileSink(s"money-$groupingIntervalLength.txt"))

  private def findMoneyAsync(content: String) =
    Future {
      findMoney(roundToIntervalStart)(content)
    }

  private def stringifyOccurrences(occurrences: Occurrences[Interval]): List[String] =
    occurrences.map(stringifyOccurrence).toList

  private def stringifyOccurrence(occurrence: Occurrence[Interval]): String =
    s"${occurrence._1} ${occurrence._2}"

}
