package pl.edu.agh.pjn2018

final case class FileContent(items: List[Item])

final case class Item(
  textContent: String,
  referencedRegulations: List[Regulation],
  judgmentDate: String
)

final case class Regulation(
  journalTitle: String,
  text: String
)
