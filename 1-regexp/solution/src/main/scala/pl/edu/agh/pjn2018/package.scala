package pl.edu.agh

package object pjn2018 {

  type Occurrence[T] = (T, Int)
  type Occurrences[T] = Map[T, Int]
  type Interval = BigDecimal

  def emptyOccurrences[T] = Map.empty[T, Int]

}
