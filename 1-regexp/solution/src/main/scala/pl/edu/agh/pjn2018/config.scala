package pl.edu.agh.pjn2018

import java.io.File

final case class ProcessingConfig(
  task: ProcessingTask,
  fromYear: Int,
  dataRoot: File
)

sealed trait ProcessingTask

object ProcessingTask {

  case object GroupMoney extends ProcessingTask

  case object CountReferencesToRegulation extends ProcessingTask

  case object CountWordOccurrences extends ProcessingTask

  case object CountMoneyOccurrences extends ProcessingTask

}
