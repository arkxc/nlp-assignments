package pl.edu.agh.pjn2018

import java.io.File

import io.circe._
import io.circe.generic.auto._
import io.circe.parser._

import scala.concurrent.{ExecutionContext, Future}

object JsonParser {

  def fromFileAsync(file: File)(implicit ec: ExecutionContext): Future[Either[Error, FileContent]] =
    Future {
      val src = scala.io.Source.fromFile(file)
      val content = src.getLines().mkString("\n")
      val parsed = decode[FileContent](content)
      src.close()
      parsed
    }

}
