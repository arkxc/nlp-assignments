package pl.edu.agh.pjn2018

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer

import scala.concurrent.ExecutionContext

trait StreamLauncher {

  implicit val sys: ActorSystem
  implicit val mat: ActorMaterializer
  implicit val ec : ExecutionContext

}
