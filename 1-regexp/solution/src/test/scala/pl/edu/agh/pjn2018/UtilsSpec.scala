package pl.edu.agh.pjn2018

import org.scalatest.{FlatSpec, Inspectors, Matchers}

class UtilsSpec extends FlatSpec with Matchers with Inspectors {

  import Utils._
  import UtilsSpec._

  val findMoneyFun: String => Occurrences[Interval] = findMoney(identity)

  "moneyStringToValue" should "convert strings to correct numerical value" in {
    val inputs = madeUpExamples.keysIterator flatMap ultimateMoneyRegex.findAllMatchIn
    val results = inputs map moneyMatchToValue
    val expectations = madeUpExamples.valuesIterator
    val resultsWithExpectations = (results zip expectations).toList

    forAll(resultsWithExpectations) { case (result, expectation) => result shouldEqual expectation }
  }

  "findMoney" should "catch all simple real examples" in new VerifyExamples(simpleRealExamples)
  it should "catch all complex real examples" in new VerifyExamples(complexRealExamples)
  it should "catch all net and gross real examples" in new VerifyExamples(netGrossRealExamples)
  it should "catch nothing in examples with missing money" in new VerifyExamples(missingMoneyRealExamples)


  class VerifyExamples(examples: Examples) {
    private val inputs                  = examples.keysIterator
    private val results                 = inputs.map(findMoneyFun).map(_.keysIterator.toList)
    private val expectations            = examples.valuesIterator
    private val resultsWithExpectations = (results zip expectations).toList

    forAll(resultsWithExpectations) { case (result, expectation) => result shouldEqual expectation }
  }

}

object UtilsSpec {

  type Examples = Map[String, List[BigDecimal]]

  val madeUpExamples = Map(
    "1234 zł" -> 1234,
    "3 tys. złotych" -> 3000,
    "56.789 złotych" -> 56789,
    "987 654 złote" -> 987654,
    "1.000.000 zł." -> 1000000,
    "1 000 000 zł" -> 1000000,
    "1 mln złotych" -> 1000000,
    "5 mln. zł." -> 5000000
  )

  val simpleRealExamples: Examples = Map(
    "woty\n64 600 zł z ty" -> List(64600),
    "i,\nwynosiła 64 600 zł. W dniu 1 grudnia 1991 r. po" -> List(64600),
    "którego wartość rynkowa wynosi 150.000 zł.\nWymagania stawiane członkowi" -> List(150000),
    "Państwa wynagrodzenie w kwocie\n1.800 zł, a więc połowę stawki minimalnej" -> List(1800),
    "premii gwarancyjnej od\npaństwa w kwocie 7.804,81 zł. Aktualnie " -> List(BigDecimal(780481, 2)),
    "powoda o około 200.000 złotych. Wypełnienie" -> List(200000),
    "rzecz od pozwanej kwoty 150.000 zł jako odszkodowania na\nzakup mieszkania" -> List(150000),
    "decyzją, na kwotę 10.828,20 zł.\nU" -> List(BigDecimal(1082820, 2)),
    "zał kwotę 31,07 zł tytułem częściowej rea" -> List(BigDecimal(3107, 2)),
    "wartość\nprzedmiotu zaskarżenia na kwotę 100 000 zł – identycznie" -> List(100000),
    "nieruchomości (40 000 zł po denominacji) oraz kwoty powyżej 127 988 zł z tytułu" -> List(40000, 127988),
    "lił\nnakaz zapłaty co do kwoty 83.383,36 zł.\n" -> List(BigDecimal(8338336, 2)),
    "zadośćuczynienia <br/>w wysokości 50 000 złotych, zaskarżony" -> List(50000),
    "zasądzenie na jej rzecz kwoty 5.895.502,17\nzłotych. W toku procesu" -> List(BigDecimal(589550217, 2)),
    "inwestycji w wysokości 6.200.000,- złotych. Wskazał" -> List(6200000),
    "Gminie z wyjątkiem sumy\n304.497,83 złotych, którą przelano" -> List(BigDecimal(30449783, 2)),
    "Spółce, m.in., kwotę 371.272,- złotych\ntytułem odsetek" -> List(371272),
    "powództwa w kwocie 190.000 zł oraz o wy-\nrównanie" -> List(190000),
    "kwotę 120 zł tytułem" -> List(120)
  )

  val complexRealExamples: Examples = Map(
    "spółki. W 1993 r. spółka poniosła stratę w wysokości 1.174.653.300,00 zł. Jej\n" +
      "aktywa zaś wynosiły 1.499.396.870,00 zł. W okresie od 2 stycznia do 30 czerwca\n" +
      "1994 r. spółka wykazywała dalszą stratę w wysokości 397.297.000,00 zł. Z końcem\n" +
      "czerwca 1994 r. wartość zobowiązań spółki wynosiła 1.571.950.300,00 zł, co\n" +
      "przekroczyło wartość jej aktywów. Spółka wprowadziła na polski obszar celny\n" +
      "towary, które nie podlegały zwolnieniu od cła. W związku" ->
      List(1174653300, 1499396870, 397297000, 1571950300),

    "od dnia 1 stycznia 2000 r. na kwotę\n" +
      "211.079,23 zł. Wyrok uprawomocnił się w dniu 5 marca 2003 r. W dniu 24 lipca\n2003 r." ->
      List(BigDecimal(21107923, 2)),

    "zasądza od powoda na rzecz pozwanej kwotę 5.400,-\n" +
      "(pięć tysięcy czterysta) złotych tytułem zwrotu kosztów\n" +
      "procesu za instancję kasacyjną" ->
      List(5400),

    "Protokolant Anna Banasiuk\n" +
      "w sprawie z powództwa Skarbu Państwa - Ministra Gospodarki i Pracy - obecnie\n" +
      "Ministra Pracy i Polityki Społecznej\n" +
      "przeciwko Gminie Miasto S.\n" +
      "o zapłatę,\n" +
      "po rozpoznaniu na rozprawie w Izbie Cywilnej w dniu 14 lutego 2006 r.,\n" +
      "skargi kasacyjnej strony powodowej od wyroku Sądu Apelacyjnego\n" +
      "z dnia 20 maja 2005 r., sygn. akt [...],\n" +
      "1. uchyla zaskarżony wyrok w pkt I oraz wyrok Sądu\n" +
      "Okręgowego w S. z dnia 21 stycznia 2005 r., sygn. akt [...] w\n" +
      "punkcie I w części dotyczącej powództwa o zapłatę kwoty\n" +
      "2.470.664,63 (dwa miliony czterysta siedemdziesiąt tysięcy\n" +
      "sześćset sześćdziesiąt cztery 63/100) złotych i w tej części\n" +
      "odrzuca pozew;\n2." ->
      List(BigDecimal(247066463, 2))
  )

  val netGrossRealExamples: Examples = Map(
    "Należność z tytułu wynagrodzenia\nwyliczył na kwotę 904 977,10 zł (853889,39 - 8116,40 + 7% VAT)." ->
      List(BigDecimal(90497710, 2)),

    "na kwotę 125 009,31 zł netto\n(133 759,96 brutto), co" ->
      List(BigDecimal(12500931, 2)),

    "Całkowita wartość robót objętych zamówieniem określona została na kwotę 5 705\n195, 70 zł netto." ->
      List(BigDecimal(570519570, 2)),

    "W dniu 18 grudnia 2002 r. powód wystawił pozwanej za wykonane na jej\n" +
      "rzecz roboty budowlane fakturę nr 232/2002 na kwotę 853 889,39 zł netto\n" +
      "(913 661,65 zł brutto) z tytułu rozliczenia końcowego." ->
      List(BigDecimal(85388939, 2), BigDecimal(91366165, 2))
  )

  val missingMoneyRealExamples: Examples = List(
    "W związku z tym w dniu 22 lipca 1996 r.\nDyrektor Urzędu",

    "niu\n4\n23 lutego 1996 r. Nal",

    "te na\nart. 299 § 1 k.s.h. jest w ",

    "mianowicie art. 298 § 1 k.h. w zw. z art. 83 ust. 3\n" +
      "i ust. 5 pkt 2 ustawy z dnia 28 grudnia 1989 r. - Prawo celne, art. 298 § 2 k.h., art.\n" +
      "118 k.c. w zw. z art. 123 § 1 k.c., a także naruszenie przepisów postępowania,\n" +
      "które miało istotny wpływ na wynik sprawy, mianowicie art. 1 k.p.c. i art. 379 pkt 1\nk.p.c.",

    "wyrok SN z dnia 18 marca 2004 r., III CK\n351/02, PUG 2004, nr 12).",

    "(wyrok SN z dnia 29 kwietnia 1998 r., I CKN\n654/97, OSP 1999, nr 1, poz. 6)",

    "zawarta umowa dzierżawy na okres 10 lat. Umowa\nta",

    "Z tych względów zażalenie uległo oddaleniu (art. 39814\n" +
      "k.p.c. w zw. z art.\n39821\nk.p.c. i art. 3941\n§ 3 k.p.c.).\n",

    "stanowiącej działkę 40/1 o pow. 494 m2\ni ustalenie",

    "(por. np. orzeczenia Sądu Najwyższego z dnia 8 października\n" +
      "1997 r., l CKN 312/97, z 19 lutego 2002 r., IV CKN 718/00, z dnia 18 marca 2003 r.,\n" +
      "IV CKN 11862/00, z dnia 20 lutego 2003 r., l CKN 65/01, z dnia 22 maja 2003 r.,\n" +
      "II CKN 121/01, nie publikowane i z dnia 10 listopada 1998 r., III CKN 792/98.0SNC\n1999, nr 4, poz. 83).",

    "rozstrzygnięcia danej sprawy przez sąd (wyroki TK z: 9 czerwca 1998 r., " +
      "K. 28/97, OTK ZU nr 4/1998, poz. 50; 16 marca 1999 r., SK 19/98, OTK ZU nr 3/1999, " +
      "poz. 36; 10 maja 2000 r., K. 21/99, OTK ZU nr 4/2000, poz. 109).\n\n" +
      "Podstawowe znaczenie dla określenia zakresu",

    "postanowieniem z dnia 30 września 2004 r. wpisał",

    "Konstytucyjnego z dnia 4 października\n2000 r., P 8/00 (OTK 2000 nr 5, poz. 189), gdzie",

    "z dnia 4 paź-\ndziernika 2000 r., U 8/00, skarżący",

    "art. 4771\n§ 1\nk.p.c. odwołał",

    "w wyro-\nku z dnia 7 sierpnia 2001 r., I PKN 563/00, zgodnie",

    "stanu na dzień\n31.12.2003 r. Jest"
  ).map(_ -> Nil).toMap

}
