name := "nlp-regexp"

version := "0.1.0"

scalaVersion := "2.12.4"

scalacOptions += "-Ypartial-unification"


libraryDependencies ++= {
  val akkaVersion = "2.5.11"
  val catsVersion = "1.0.1"
  val circeVersion = "0.9.1"
  val scalatestVersion = "3.0.5"

  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "org.typelevel" %% "cats-core" % catsVersion,
    "io.circe" %% "circe-core" % circeVersion,
    "io.circe" %% "circe-generic" % circeVersion,
    "io.circe" %% "circe-parser" % circeVersion,

    "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
    "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion % Test,
    "org.scalatest" %% "scalatest" % scalatestVersion % Test
  )
}
