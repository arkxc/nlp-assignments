from itertools import groupby

import matplotlib.pyplot as plt
import numpy as np
import os

DATA_FILE = os.path.join('solution', 'money-10000.txt')


def load_and_parse_data():
    with open(DATA_FILE, 'r') as f:
        raw_data = f.readlines()
    splitted_gen = (line.strip().split() for line in raw_data)
    return sorted((int(splitted[0]), int(splitted[1])) for splitted in splitted_gen)


def plot_data(data, step, output_file_name):
    vals, weights = zip(*data)
    bin_min = np.min(vals)
    bin_max = np.max(vals)

    plt.hist(vals, weights=weights, bins=range(bin_min, bin_max + 2*step, step))
    plt.title("Wystąpienia wartości pieniężnych")
    plt.xlabel("Wartości pieniężne")
    plt.ylabel("Liczba wystąpień")

    plt.savefig(output_file_name)


def merge_intervals(data, int_len):
    grouped = groupby(data, lambda x: x[0] // int_len * int_len)
    dropped_keys = ((key, map(lambda x: x[1], kv)) for key, kv in grouped)
    return [(key, sum(values)) for key, values in dropped_keys]


def main():
    data = load_and_parse_data()
    data = [(k, v) for k, v in data if k < 100000000]
    lt1M = [(val, weight) for val, weight in data if val <  1000000]
    ge1M = [(val, weight) for val, weight in data if val >= 1000000]

    data = merge_intervals(data, 10000000)
    ge1M = merge_intervals(ge1M, 10000000)

    #plot_data(data, 1000000, 'money-results')

    #plot_data(lt1M, 10000,   'money-lt1M-results')
    plot_data(ge1M, 1000000, 'money-ge1M-results')


if __name__ == '__main__':
    main()

