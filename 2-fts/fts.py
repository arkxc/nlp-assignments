from elasticsearch import Elasticsearch


def count_judgments(es, query):
    res = es.search(index='judgment', doc_type='judgments', body=query, size=0)
    return res['hits']['total']

def count_judgments_matching(es, word):
    q = { 'query': { 'match': { 'content': word } } }
    return count_judgments(es, q)

def count_judgments_matching_phrase(es, phrase):
    q = { 'query': { 'match_phrase': { 'content': phrase } } }
    return count_judgments(es, q)

def count_judgments_matching_phrase_with_slop(es, phrase, slop):
    q = { "query": { "match_phrase": { "content": { "query": phrase, "slop":  slop } } } }
    return count_judgments(es, q)


def main():
    es = Elasticsearch()

    print('Liczba orzeczeń, w których występuje słowo "szkoda":',
            count_judgments_matching(es, 'szkoda'))
    print()

    print('Liczba orzeczeń, w których występuje fraza "trwały uszczerbek na zdrowiu":',
            count_judgments_matching_phrase(es, 'trwały uszczerbek na zdrowiu'))
    print()

    print('Liczba orzeczeń, w których występuje fraza "trwały uszczerbek na zdrowiu"\n' +
          'z uwzględnieniem możliwości wystąpienia maksymalnie 2 dodatkowych słów\n' +
          'pomiędzy dowolnymi elementami frazy:',
            count_judgments_matching_phrase_with_slop(es, 'trwały uszczerbek na zdrowiu', 2))


if __name__ == '__main__':
    main()

