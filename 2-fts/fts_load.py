from elasticsearch import Elasticsearch, helpers
import json, os, sys

INDEX_SETTINGS = {
    "settings": {
        "index": {
            "number_of_shards":   1,
            "number_of_replicas": 0
        },
        "analysis": {
            "analyzer": {
                "judgment_analyzer": {
                    "tokenizer": "standard",
                    "filter":    "morfologik_stem"
                }
            }
        }
    },
    "mappings": {
        "judgments": {
            "properties": {
                "content": {
                    "type":     "text",
                    "analyzer": "judgment_analyzer"
                },
                "date": {
                    "type":     "date",
                    "format":   "yyyy-MM-dd"
                },
                "signature":    { "type": "keyword" },
                "judges":       { "type": "text"    }
            }
        }
    }
}


def build_doc(item):
    return {
        'content':   item['textContent'],
        'date':      item['judgmentDate'],
        'signature': item['courtCases'][0]['caseNumber'],
        'judges':    [j['name'] for j in item['judges']]
    }


def load_into_es(es, docs):
    actions = (
        {
            '_index': 'judgment',
            '_type':  'judgments',
            '_source': doc
        } for doc in docs
    )
    helpers.bulk(es, actions)


def potentially_load_into_es(es, data):
    docs = []
    for item in data['items']:
        year = item['judgmentDate'][:4]
        if year == '2006':
            docs.append(build_doc(item))
    if docs:
        load_into_es(es, docs)


def main():
    if len(sys.argv) < 2:
        print("Data directory path needed.")
        sys.exit(-1)

    es = Elasticsearch()
    es.indices.create(index="judgment", body=INDEX_SETTINGS)

    for dirname, _, filenames in os.walk(sys.argv[1]):
        for filename in filenames:
            if filename.startswith('judgments-') and filename.endswith('.json'):
                data = json.load(open(os.path.join(dirname, filename)))
                potentially_load_into_es(es, data)


if __name__ == '__main__':
    main()

