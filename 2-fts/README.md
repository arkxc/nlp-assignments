# Wyniki przetwarzania zbioru danych dla 2006 roku

1. Liczba orzeczeń, w których występuje słowo _szkoda_: **445**
2. Liczba orzeczeń, w których występuje fraza _trwały uszczerbek na zdrowiu_: **9**
3. Liczba orzeczeń, w których występuje fraza _trwały uszczerbek na zdrowiu_ z uwzględnieniem możliwości wystąpienia maksymalnie 2 dodatkowych pomiędzy dowolnymi elementami frazy: **10**
