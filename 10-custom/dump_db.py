import mysql.connector, re

TABLES = ["fragment_anotatora_2011_ZK", "fragment_anotatora_2011b", "fragment_anotatora_2012_luty"]

QUERY_TEMPLATE = "SELECT negatywne_emocje, ironia_sarkazm, tekst FROM {}"

def label_negemot(n):
    return "__label__negemot" + str(n)

def label_irony(is_irony):
    return "__label__irony" if is_irony else None

def clear_text(text):
    remove_tags = re.sub(r"<[^>]*>", " ", text)
    flatten_spaces = re.sub(r"\s+", " ", remove_tags)
    return flatten_spaces.strip()

def main():
    conn = mysql.connector.connect(user="root", password="password", host="127.0.0.1", database="hatedb")
    cursor = conn.cursor()

    with open("hatespeech.txt", "w") as f:
        for t in TABLES:
            q = QUERY_TEMPLATE.format(t)
            cursor.execute(q)
            for emot_rate, is_irony, text in cursor:
                labels = [label_negemot(emot_rate), label_irony(is_irony)]
                labels = (label for label in labels if label)
                labels = " ".join(labels)
                f.write("{} {}\n".format(labels, clear_text(text)))

    cursor.close()
    conn.close()

if __name__ == "__main__":
    main()
