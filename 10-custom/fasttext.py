import subprocess
from pathlib import Path

class FastText:
    def __init__(self, executable):
        self.executable = str(Path(executable).resolve())

    def train(self, train_file, model_name):
        cmd = [self.executable, "supervised", "-input", train_file, "-output", model_name]
        return subprocess.run(cmd).returncode == 0

    def test(self, model_file, test_file):
        cmd = [self.executable, "test", model_file, test_file]
        res = subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode("utf-8").split()
        return {k: v for k, v in zip(res[::2], res[1::2])}

    def predict(self, model_file, test_file):
        cmd = [self.executable, "predict", model_file, test_file]
        res = subprocess.run(cmd, stdout=subprocess.PIPE).stdout.decode("utf-8").split()
        return res

