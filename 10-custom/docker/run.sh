#!/bin/bash

#           --mount   type=bind,source="$(pwd)"/hatespeech,target=/var/lib/mysql/hatespeech \
docker run --name    hate-mysql \
           --publish 3306:3306 \
           --env     MYSQL_ROOT_PASSWORD=password \
           --detach \
           arkxc/hate-mysql:1.0.0
