from pygtrie import CharTrie
import json, os, re, sys


def words_list(content):
    remove_breaks = re.sub(r'-\n', '', content)
    remove_tags = re.sub(r'<[^>]*>', ' ', remove_breaks)
    lowered = remove_tags.lower()
    splitted = re.compile(r'\W+', re.UNICODE).split(lowered)
    return [word for word in splitted if len(word) > 1 and not re.match(r'\d+', word)]


def count_word_occurrences(data, freq_trie):
    for item in data['items']:
        year = item['judgmentDate'][:4]
        if year == '2006':
            for word in words_list(item['textContent']):
                prev = freq_trie.get(word)
                freq_trie[word] = prev + 1 if prev else 1


def persist_freq_list(freq_trie, freq_file):
    for count, word in sorted(((freq_trie[word], word) for word in freq_trie), reverse=True):
        line = '{} {}\n'.format(count, word)
        freq_file.write(line)


def main():
    if len(sys.argv) < 2:
        print('Data directory path needed.')
        sys.exit(-1)

    freq_trie = CharTrie()

    for dirname, _, filenames in os.walk(sys.argv[1]):
        for filename in filenames:
            if filename.startswith('judgments-') and filename.endswith('.json'):
                data = json.load(open(os.path.join(dirname, filename)))
                count_word_occurrences(data, freq_trie)

    with open('freq_list.txt', 'w') as freq_file:
        persist_freq_list(freq_trie, freq_file)


if __name__ == '__main__':
    main()

