# Wyniki przetwarzania zbioru danych dla 2006 roku

1. Wykres przedstawiający częstość występowania słów:

    ![](freq_plot.png)

2. Przykładowe słowa nienależące do słownika polimorfologik z proponowanymi poprawkami:

    - unormowań -> unormowań
    - sądowoadministracyjnego -> sądowoadministracyjnego
    - reformatoryjnego -> reformatoryjnego
    - ponadzakładowego -> ponadzakładowego
    - intertemporalnych -> intertemporalnych
    - preemitentów -> preemitentów
    - pierwszoinstancyjnego -> pierwszoinstancyjnego
    - autoproducentów -> autoproducentów
    - uchybionego -> chybionego
    - postulacyjnej -> postulacyjnej
    - kasatoryjnym -> kasatoryjnym
    - podustawowej -> podstawowej
    - przeciwegzekucyjnego -> przeciwegzekucyjnego
    - daninowego -> taninowego
    - międzyinstancyjnego -> międzyinstancyjnego
    - retroaktywnego -> retroaktywnego
    - reemitowanie -> remitowanie
    - prowspólnotowej -> prowspólnotowej
    - obciążliwym -> obciążliwym
    - subdelegacji -> subdelegaci
    - poddłużnika -> podłużnika
    - niespółdzielców -> niespółdzielców
    - współkorzystania -> współkorzystania
    - salwatoryjnej -> salwatoryjnej
    - przekonywujące -> przekonywające
    - egzoneracyjnych -> egzoneracyjnych
    - doręczeniach -> poręczeniach
    - faktorant -> faktorant
    - wdzwanianego -> wydzwanianego
    - ubezskutecznienie -> ubezskutecznienie

