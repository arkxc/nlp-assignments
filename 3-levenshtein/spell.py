import re, sys
from collections import Counter

def parse_freqs():
    d = {}
    with open('freq_list.txt', 'r') as f:
        for line in f.readlines():
            n, word = line.strip().split()
            d[word] = int(n)
    return d

WORDS = parse_freqs()

def P(word, N=sum(WORDS.values())): 
    "Probability of `word`."
    return WORDS.get(word, 0) / N

def correction(word): 
    "Most probable spelling correction for word."
    return max(candidates(word), key=P)

def candidates(word): 
    "Generate possible spelling corrections for word."
    return (known([word]) or known(edits1(word)) or known(edits2(word)) or [word])

def known(words):
    "The subset of `words` that appear in the dictionary of WORDS."
    found = []
    with open(sys.argv[1], 'r') as f:
        for line in f.readlines():
            form = line.split(";")[1]
            if form in words:
                found.append(form)
    return set(found)

def edits1(word):
    "All edits that are one edit away from `word`."
    letters    = 'abcdefghijklmnopqrstuvwxyz'
    splits     = [(word[:i], word[i:])    for i in range(len(word) + 1)]
    deletes    = [L + R[1:]               for L, R in splits if R]
    transposes = [L + R[1] + R[0] + R[2:] for L, R in splits if len(R)>1]
    replaces   = [L + c + R[1:]           for L, R in splits if R for c in letters]
    inserts    = [L + c + R               for L, R in splits for c in letters]
    return set(deletes + transposes + replaces + inserts)

def edits2(word): 
    "All edits that are two edits away from `word`."
    return (e2 for e1 in edits1(word) for e2 in edits1(e1))

def main():
    with open('not_found_input.txt', 'r') as in_file:
        with open('corrected.txt', 'w') as out_file:
            for in_line in in_file.readlines():
                in_line = in_line.strip()
                print('correcting: ' + in_line + '\n')
                out_line = in_line + ' -> ' + correction(in_line) + '\n'
                out_file.write(out_line)

if __name__ == '__main__':
    main()

