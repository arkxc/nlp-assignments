from pygtrie import CharTrie
import sys


def main():
    if len(sys.argv) < 3:
        print('Dictionary and frequency list file paths needed.')
        sys.exit(-1)

    dict_trie = CharTrie()
    with open(sys.argv[1], 'r') as dict_file:
        for line in dict_file.readlines():
            word = line.split(';')[1].lower()
            dict_trie[word] = True

    with open(sys.argv[2], 'r') as freq_file:
        with open('not_found.txt', 'w') as not_found_file:
            for line in freq_file.readlines():
                word = line.split()[1]
                if not dict_trie.get(word, False):
                    not_found_file.write(word + '\n')


if __name__ == '__main__':
    main()

