from itertools import groupby

import matplotlib.pyplot as plt
import numpy as np


def load_and_parse_data():
    with open('freq_list.txt', 'r') as f:
        raw_data = f.readlines()
    freq_only = [int(line.strip().split()[0]) for line in raw_data]
    return freq_only


def plot_data(freqs, output_file_name):
    plt.loglog(range(len(freqs)), freqs)

    plt.title("Częstość występowania słów")
    plt.xlabel("Pozycja słowa na liście frekwencyjnej")
    plt.ylabel("Liczba wystąpień słowa zajmującego określoną pozycję")

    plt.savefig(output_file_name)


def main():
    freqs = load_and_parse_data()
    plot_data(freqs, 'freq_plot')


if __name__ == '__main__':
    main()

