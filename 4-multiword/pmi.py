from math import log2
from pygtrie import CharTrie
from operator import itemgetter

ALL_UNI = 4465167
ALL_BI  = 4462792

def fill_uni_trie():
    uni_trie = CharTrie()

    with open('unigram_freq.txt', 'r') as f:
        for line in f.readlines():
            splitted = line.split()
            uni_trie[splitted[1]] = int(splitted[0])

    return uni_trie


def count_and_collect_pmi(in_file, out_dict, uni_trie):
    for line in in_file.readlines():
        splitted = line.split()
        bigram = splitted[1] + ' ' + splitted[2]
        print('Processing: ' + bigram)
        pxy = int(splitted[0]) / ALL_BI
        px  = uni_trie.get(splitted[1], 0) / ALL_UNI
        py  = uni_trie.get(splitted[2], 0) / ALL_UNI
        pmi = log2(pxy / (px * py))
        out_dict[bigram] = pmi


def main():
    uni_trie = fill_uni_trie()
    out_dict = {}

    with open('bigram_freq.txt', 'r') as in_file:
        count_and_collect_pmi(in_file, out_dict, uni_trie)

    with open('bigram_pmi.txt', 'w') as out_file:
        for bigram, count in sorted(out_dict.items(), key=itemgetter(1), reverse=True):
            out_file.write('{} {}\n'.format(count, bigram))


if __name__ == '__main__':
    main()

