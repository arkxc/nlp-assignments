# Wyniki przetwarzania zbioru danych dla 2006 roku

1. Bigramy o największej punktowej informacji wzajemnej (22.091): 

    - żywą bolesnością
    - żydowskich międzykościelną
    - żużla wielkopiecowego
    - żniwach wykopkach
    - święte krowy
    - świąteczno noworocznym
    - świateł awaryjnych
    - śródnaczyniową tętniaków
    - średnicą główki
    - śpiączkę mózgową
    - śnieg wiał
    - ślepym zaułku
    - śladowej bodaj
    - ściankę działową
    - łóżkami lżejszych
    - łaźnią drewnianą
    - łańcuch rozpada
    - ławy królewskiej
    - łacińskiego facsimile
    - złoże piaszczysto
    - złości wyrzucał
    - złogami wapnia
    - złapał pasażerkę
    - zwłóknienie śródmiąższowe
    - zwyrodnieniowo zniekształcające
    - zwyrodnienie tłuszczowe
    - zwyczajną przewidywalną
    - zostawała bieżąca
    - zniesławiać oskarżać
    - znakowi towarowemu

2. Bigramy o największej wartości wzoru na statystykę logarytmiczną opartą o rozkład dwumienny:

    - art ust
    - k p
    - art k
    - z dnia
    - nr poz
    - p c
    - dz u
    - sąd najwyższy
    - na podstawie
    - trybunał konstytucyjny
    - ze zm
    - grudnia r
    - sądu najwyższego
    - sąd apelacyjny
    - sąd okręgowy
    - ubezpieczeń społecznych
    - sygn akt
    - k c
    - związku z
    - skargi kasacyjnej
    - sądu okręgowego
    - stycznia r
    - w sprawie
    - z art
    - pierwszej instancji
    - czerwca r
    - trybunału konstytucyjnego
    - poz ze
    - u nr
    - zgodnie z

