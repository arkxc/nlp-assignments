def sum_occurrences(filename):
    occ_sum = 0
    with open(filename, 'r') as f:
        for line in f.readlines():
            count = line.split()[0]
            occ_sum += int(count)
    return occ_sum


def main():
    print('Number of unigrams: {}'.format(sum_occurrences('unigram_freq.txt')))
    print('Number of bigrams:  {}'.format(sum_occurrences('bigram_freq.txt')))


if __name__ == '__main__':
    main()

