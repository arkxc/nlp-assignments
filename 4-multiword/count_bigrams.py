from pygtrie import CharTrie
import json, os, re, sys


def tokenize(content):
    remove_breaks = re.sub(r'-\n', '', content)
    remove_tags = re.sub(r'<[^>]*>', ' ', remove_breaks)
    lowered = remove_tags.lower()
    splitted = re.compile(r'\W+', re.UNICODE).split(lowered)
    return [word for word in splitted if len(word) > 0 and not re.match(r'\d+', word)]


def count_bigrams(data, freq_trie):
    for item in data['items']:
        year = item['judgmentDate'][:4]
        if year == '2006':
            words_list = tokenize(item['textContent'])
            for fst, snd in zip(words_list, words_list[1:]):
                bigram = fst + ' ' + snd
                prev = freq_trie.get(bigram)
                freq_trie[bigram] = prev + 1 if prev else 1


def persist_freq_list(freq_trie, freq_file):
    for count, bigram in sorted(((freq_trie[bigram], bigram) for bigram in freq_trie), reverse=True):
        line = '{} {}\n'.format(count, bigram)
        freq_file.write(line)


def main():
    if len(sys.argv) < 2:
        print('Data directory path needed.')
        sys.exit(-1)

    freq_trie = CharTrie()

    for dirname, _, filenames in os.walk(sys.argv[1]):
        for filename in filenames:
            if filename.startswith('judgments-') and filename.endswith('.json'):
                data = json.load(open(os.path.join(dirname, filename)))
                count_bigrams(data, freq_trie)

    with open('bigram_freq.txt', 'w') as freq_file:
        persist_freq_list(freq_trie, freq_file)


if __name__ == '__main__':
    main()

